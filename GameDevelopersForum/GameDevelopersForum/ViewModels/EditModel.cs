﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameDevelopersForum.ViewModels
{
	public class EditModel
	{
		[Remote(action: "CheckPassword", controller: "Account", ErrorMessage = "Неверный пароль")]
		[DataType(DataType.Password)]
		public string LastPassword { get; set; }

		[DataType(dataType: DataType.Password)]
		public string NewPassword { get; set; }

		[DataType(DataType.Password)]
		[Compare("NewPassword", ErrorMessage = "Пароль введен неверно")]
		public string ConfirmPassword { get; set; }

		[DataType(DataType.EmailAddress) ]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }
		public string About { get; set; }
	}
}
