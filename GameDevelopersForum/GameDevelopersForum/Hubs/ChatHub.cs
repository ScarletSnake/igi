﻿using GameDevelopersForum.Controllers;
using GameDevelopersForum.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameDevelopersForum.Hubs
{
	public class ChatHub : Hub
	{
		ForumContext db;
        private readonly ILogger logger;

        public ChatHub(ForumContext db, ILogger logger) : base()
		{
			this.db = db;
            this.logger = logger;
		}

		public async Task SendMessage(string user, string message)
		{
            logger.LogInformation($"Пользователь {user} написал сообщение: '{message}'");
            Message mess = new Message { User = db.Users.FirstOrDefault(u => u.Login == user), Text = message, Time = DateTime.Now};
            await Clients.All.SendAsync("ReceiveMessage", user, message);
            db.Messages.Add(mess);
            await db.SaveChangesAsync();
            logger.LogInformation($"Сообщение: '{message}' , успешно сохранено в базу");
        }
	}
}
