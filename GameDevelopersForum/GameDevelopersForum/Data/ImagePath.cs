﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameDevelopersForum.Data
{
    public class ImagePath
    {
        private static readonly string GlobalImagePath = "/images/Users/";

        public string UserImagePath { get; }
        
        public ImagePath(string imagePath)
        {
            UserImagePath = GlobalImagePath + imagePath;
        }
    }
}
