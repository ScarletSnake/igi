﻿using GameDevelopersForum.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameDevelopersForum.Service.Implm
{
    public class CheckerEmail : ICheckerEmail
    {
        public bool CheckEmail(string email, ForumContext context)
        {
            var users = context.Users.ToList();
            foreach (var user in users)
                if (user.Email == email)
                    return false;
            return true;
        }
    }
}
