﻿using GameDevelopersForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameDevelopersForum.Service
{
    public interface ICheckerEmail
    {
        bool CheckEmail(string email, ForumContext context);
    }
}
