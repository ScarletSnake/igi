﻿using GameDevelopersForum.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameDevelopersForum.Controllers
{
	public class ForumController : Controller
	{
		private readonly ForumContext db;
        private readonly ILogger logger;

        public ForumController(ForumContext db, ILogger<ForumController> logger)
		{
            this.logger = logger;
			this.db = db;
		}

		[Authorize]
		public IActionResult Create()
		{
			Topic topic = new Topic
			{
				CreationDate = DateTime.Now,
				User = db.Users.FirstOrDefault(u => u.Login == User.Identity.Name),
			};

			db.Topics.Add(topic);
			db.SaveChanges();

            logger.LogInformation("Запуск создания форума");
            return RedirectToAction("TopicEdit", new RouteValueDictionary(new
            {
                controller = "DataBases", action = "TopicEdit", Id = topic.Id
            }));

		}

		public IActionResult List()
		{
			return View(db.Topics.Take(countTopicsToShow).ToList());
		}

        [HttpPost]
        public async Task<IActionResult> Send(int id, string text)
        {
            try
            {
                User user = db.Users.First(u => u.Login == User.Identity.Name);
                Topic topic = db.Topics.Find(id);
                Message message = new Message { Text = text, User = user, Time = DateTime.Now, Topic = topic };
                db.Messages.Add(message);
                await db.SaveChangesAsync();
                logger.LogInformation($"Пользователь с id: {user.Id} написал текст: '{text}' для форума с id: {id}");
                return RedirectToAction($"/{id}", "/forum");
            }

            catch
            {
                logger.LogInformation($"Пользователь, не вошедший в систему, пытался написать текст: '{text}' для форума с id: {id}");
                return BadRequest("Дубина, ты ж не зарегался!");
            }
        }

        public IActionResult Show(int id)
        {
            Topic topic = db.Topics.Find(id);
            ViewBag.User = db.Users.Find(topic.UserId);
            IQueryable<Message> messages = db.Messages.Where(m => m.TopicId == id);
            foreach (var message in messages)
            {
                User user = db.Users.Where(u => u.Id == message.UserId).First();
                message.User = user;
            }
            ViewBag.Messages = messages;
            return View(topic);
        }

        [HttpGet]
		public async Task<IActionResult> TopicList(int pageNumber = 0)
		{
			if (Request.Headers["X-Requested-With"] == "XMLHttpRequest")
			{
				return PartialView("~/Views/Forum/ListGet.cshtml", await GetTopicList(pageNumber));
			}
			else return View(await GetTopicList(pageNumber));
		}

		const int countTopicsToShow = 5;

        private async Task<List<Topic>> GetTopicList(int pageNumber)
        {
            var topics = db.Topics;

            return topics.Skip(pageNumber * countTopicsToShow).Take(countTopicsToShow).ToList();
        }
    }
}
