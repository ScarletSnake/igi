﻿using GameDevelopersForum.Models;
using GameDevelopersForum.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameDevelopersForum.Controllers
{
	public class NewsController : Controller
	{
		private readonly ForumContext db;
        private readonly ILogger logger;

        public NewsController(ForumContext db, ILogger<NewsController> logger)
		{
			this.db = db;
            this.logger = logger;
        }
		
		public IActionResult List()
		{
			List <News> news = db.News.OrderByDescending(x => x.PublishingDate).ToList<News>();
			return View(news);
		}

		[Authorize(Roles = "admin, moderator")]
		public IActionResult Create()
		{
			News news = new News { PublishingDate = DateTime.Now};

			db.News.Add(news);
			db.SaveChanges();

            logger.LogInformation("Запуск создания новости");
            return RedirectToAction("NewsEdit", new RouteValueDictionary(
	            new {
                    controller = "DataBases", action = "NewsEdit", Id = news.Id
                }));
		
		}
		public IActionResult Show(int id)
		{
			return View(db.News.Find(id));
		}
	}
}
