﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using GameDevelopersForum.ViewModels;
using GameDevelopersForum.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System;
using GameDevelopersForum.Service;
using GameDevelopersForum.Data;
using Microsoft.Extensions.Logging;

namespace GameDevelopersForum.Controllers
{
	public class AccountController : Controller
	{
		private readonly ForumContext db;
		IHostingEnvironment appEnvironment;
        ICheckerEmail checkerEmail;
        private readonly ILogger logger;

        public AccountController(IHostingEnvironment hostingEnvironment, ForumContext context, ICheckerEmail checkerEmail, ILogger<AccountController> logger)
		{
			db = context;
			appEnvironment = hostingEnvironment;
            this.checkerEmail = checkerEmail;
            this.logger = logger;
		}

		[HttpGet]
		public IActionResult Register()
		{
            logger.LogInformation("Запуск регистрации");
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Register(RegisterModel model)
		{
			if (ModelState.IsValid)
			{
				User user = db.Users.FirstOrDefault(u => u.Login == model.Login);
                if (user == null)
                {
                    if (checkerEmail.CheckEmail(model.Email, db))
                    {
                        user = new User { Login = model.Login, Password = model.Password, Email = model.Email, About = model.About, RegistrationDate = DateTime.Now };
                        Role userRole = db.Roles.FirstOrDefault(r => r.Name == "user");

                        if (userRole != null)
                            user.Role = userRole;
                        db.Users.Add(user);

                        db.SaveChanges();
                        await Authenticate(user);

                        logger.LogInformation("Регистрация прошла успешно");
                        return RedirectToAction("Index", "Home");
                    }
                    else
                        ModelState.AddModelError("Email", "Пользователь с данным email уже существует");
                }
                else
                    ModelState.AddModelError("Login", "Пользователь с данным именем уже существует");
			}
            logger.LogInformation("Регистрация не прошла успешно, пользователь ввёл некорректные данные");
            return View(model);
		}


		[HttpGet]
		[Authorize]
		public IActionResult Edit()
		{
			User curUser = db.Users.First(u => u.Login == User.Identity.Name);
			ViewBag.user = curUser;
            logger.LogInformation("Запуск изменеия данных регистрации");
            return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Edit(EditModel model)
		{
			User user = db.Users.FirstOrDefault(u => u.Login == User.Identity.Name);
			if (ModelState.IsValid)
			{
				if (model.LastPassword == user.Password)
				{
                    if (checkerEmail.CheckEmail(model.Email, db))
                    {
                        if (model.About != null)
                        {
                            user.About = model.About;
                        }
                        if (model.Email != null)
                        {
                            user.Email = model.Email;
                        }
                        if (model.NewPassword != null)
                        {
                            user.Password = model.NewPassword;
                        }

                        db.SaveChanges();
                        logger.LogInformation("Изменения прошли успешно");

                        return RedirectToAction("Index", "Home");
                    }
                    else
                        ModelState.AddModelError("Email", "Пользователь с данным именем уже существует");
                }
                else
					ModelState.AddModelError("", "Неверно введен пароль");
			}
			ViewBag.user = user;
            logger.LogInformation("Изменения не прошли успешно, пользователь ввёл некорректные данные");
            return View();
		}

		public IActionResult Profile()
		{
			User user = db.Users.FirstOrDefault(l => User.Identity.Name == l.Login);
            logger.LogInformation("Вход в профайл");
            return View(user);
		}

		[HttpGet]
		public IActionResult Login()
		{
            logger.LogInformation("Запуск входа пользователем в учётную запись");
            return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(LoginModel model)
		{
			if (ModelState.IsValid)
			{
				User user = await db.Users
					.Include(u => u.Role)
					.FirstOrDefaultAsync(u => u.Login == model.Login && u.Password == model.Password);
				if (user != null)
				{
					await Authenticate(user);
                    logger.LogInformation("Вход выполнен успешно");
                    return RedirectToAction("Index", "Home");
				}
				ModelState.AddModelError("", "Некорректные логин и(или) пароль");
			}
            logger.LogInformation("Вход не выполнен успешно, пользователь ввёл некорректные данные");
            return View(model);
		}

		[HttpPost]
		public async Task<IActionResult> AddProfileImage(IFormFile uploadedFile)
		{
			User user = db.Users.FirstOrDefault(l => User.Identity.Name == l.Login);
            ImagePath path = new ImagePath(user.Login);
			using (var fileStream = new FileStream(appEnvironment.WebRootPath + path.UserImagePath, FileMode.Create))
			{
				await uploadedFile.CopyToAsync(fileStream);
			}
			if (uploadedFile != null)
			{
				user.imageName = uploadedFile.Name;
				await db.SaveChangesAsync();
                logger.LogInformation("Добавление выполнено успешно");
                return View();
			}
			else
			{
                logger.LogInformation("Добавление не выполнено успешно, загружаемый файл не найден");
                return View();
			}
		}

		[HttpPost]
		private async Task Authenticate(User user)
		{
			await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
			var claims = new List<Claim>
			{
				new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
				new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name)
			};
			ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
				ClaimsIdentity.DefaultRoleClaimType);
			await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
		}

		public async Task<IActionResult> Logout()
		{
			await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            logger.LogInformation("Выход из учётной записи выполнен успешно");
            return RedirectToAction("Index", "Home");
		}
	}
}