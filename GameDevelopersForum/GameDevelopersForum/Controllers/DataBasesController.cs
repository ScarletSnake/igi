﻿using GameDevelopersForum.Models;
using GameDevelopersForum.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameDevelopersForum.Controllers
{
	public class DataBasesController : Controller
	{
		private readonly ForumContext db;
        private readonly ILogger logger;

        public DataBasesController(ForumContext db, ILogger<DataBasesController> logger)
		{
			this.db = db;
            this.logger = logger;
		}

		[Authorize(Roles = "admin, moderator")]
		public IActionResult Index()
		{
			return View();
		}

		[Authorize(Roles = "admin, moderator")]
		public IActionResult News()
		{
			return View(db.News.ToList());
		}

		[Authorize(Roles = "admin")]
		public IActionResult Users()
		{
			return View(db.Users.ToList());
		}
		[Authorize(Roles = "admin, moderator")]
		public IActionResult Topics()
		{
			return View(db.Topics.ToList());
		}

        [Authorize(Roles = "admin, moderator")]
        public IActionResult Messages()
        {
            return View(db.Messages.ToList());
        }

        [HttpGet]
		public IActionResult TopicEdit(int Id)
		{
			Topic topic = db.Topics.Find(Id);
			if (User.IsInRole("admin") || User.IsInRole("moderator") || (User.Identity.IsAuthenticated && topic.Name == null))
			{
                ViewBag.CurrentTopic = topic;
				return View();
			}
			else
			{
                logger.LogWarning("Попытка входа обычным пользователем");
                return View(Content("Access denied"));
			}
		}

		[Authorize(Roles = "admin, moderator")]
		public IActionResult TopicDelete(int id)
		{
            List<Message> messages = db.Messages.Where(m => m.TopicId == id).ToList();
            foreach (var message in messages)
                db.Messages.Remove(message);
			db.Topics.Remove(db.Topics.Find(id));
			db.SaveChanges();
            logger.LogInformation($"Форум с id: {id} был успешно удалён");
            return RedirectToAction("Topics", "DataBases");
		}

		[Authorize(Roles = "admin, moderator")]
		public IActionResult NewsDelete(int id)
		{
			db.News.Remove(db.News.Find(id));
			db.SaveChanges();
            logger.LogInformation($"Новость с id: {id} была успешно удалена");
            return RedirectToAction("News", "DataBases");
		}

		[Authorize(Roles = "admin")]
		public IActionResult UserDelete(int id)
		{
			db.Users.Remove(db.Users.Find(id));
			db.SaveChanges();
            logger.LogInformation($"Пользователь с id: {id} была успешно удалён");
            return RedirectToAction("Users", "DataBases");
		}

		[ValidateAntiForgeryToken]
		[HttpPost]
		public IActionResult TopicEdit(TopicEditModel model, int id)
		{
			Topic topic = db.Topics.Find(id);
			
			if (ModelState.IsValid)
			{
				if (model.Description != null)
				{
					topic.Description = model.Description;
				}
				if (model.Name != null)
				{
					topic.Name = model.Name;
				}
				db.SaveChanges();
                logger.LogInformation($"Форум с id: {id} был успешно изменён");
                return RedirectToAction("List", "Forum");
			}
			ViewBag.CurrentTopic = topic;
            logger.LogInformation($"Форум с id: {id} не был успешно изменён: были введены некорректные данные");
            return View();
		}

		[HttpGet]
		[Authorize(Roles = "admin, moderator")]
		public IActionResult NewsEdit(int Id)
		{
			News news = db.News.Find(Id);
			ViewBag.CurrentNews = news;
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult NewsEdit(NewsEditModel model, int id)
		{
			News news = db.News.Find(id);

			if (ModelState.IsValid)
			{
				if (model.Description != null)
				{
					news.Description = model.Description;
				}
				if (model.Info != null)
				{
					news.Info = model.Info;
				}
				if (model.Name != null)
				{
					news.Name = model.Name;
				}
				db.SaveChanges();
                logger.LogInformation($"Новость с id: {id} была успешно изменена");
                return RedirectToAction("News", "DataBases");
			}
			ViewBag.CurrentTopic = news;
            logger.LogInformation($"Новость с id: {id} не была успешно изменена: были введены некорректные данные");
            return View();
		}

		public IActionResult UserAPI()
		{
			return View();
		}

		[HttpGet]
		[Authorize(Roles = "admin")]
		public IActionResult UserEdit(int Id)
		{
			User user = db.Users.Find(Id);
			ViewBag.CurrentUser = user;
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult UserEdit(UserEditModel model, int id)
		{
			User user = db.Users.Find(id);

			if (ModelState.IsValid)
			{
				if (model.Login != null)
				{
					user.Login = model.Login;
				}
				if (model.Email != null)
				{
					user.Email = model.Email;
				}
				if (model.About != null)
				{
					user.About = model.About;
				}
				if (model.Password != null)
				{
					user.Password = model.Password;
				}

				if (model.RoleId != null)
				{
					user.RoleId = model.RoleId;
				}
				db.SaveChanges();
                logger.LogInformation($"Пользователь с id: {id} был успешно изменён");
                return RedirectToAction("Users", "DataBases");
			}
			ViewBag.CurrentUser = user;
            logger.LogInformation($"Пользователь с id: {id} не был успешно изменён: были введены некорректные данные");
            return View();
		}

        [HttpGet]
        public IActionResult MessageEdit(int Id)
        {
            Message message = db.Messages.Find(Id);
            if (User.IsInRole("admin") || User.IsInRole("moderator") || (User.Identity.IsAuthenticated && message.Text == null))
            {
                ViewBag.CurrentMessage = message;
                return View();
            }
            else
            {
                logger.LogWarning("Попытка входа обычным пользователем");
                return View(Content("Access denied"));
            }
        }

        [Authorize(Roles = "admin, moderator")]
        public IActionResult MessageDelete(int id)
        {
            db.Messages.Remove(db.Messages.Find(id));
            db.SaveChanges();
            logger.LogInformation($"Сообщение с id: {id} было успешно удалено");
            return RedirectToAction("Messages", "DataBases");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult MessageEdit(MessagesEditModel model, int id)
        {
            Message message = db.Messages.Find(id);

            if (ModelState.IsValid)
            {
                if (model.Text != null)
                {
                    message.Text = model.Text;
                }
                db.SaveChanges();
                logger.LogInformation($"Сообщение с id: {id} было успешно изменено");
                return RedirectToAction("Messages", "DataBases");
            }
            ViewBag.CurrentMessage = message;
            logger.LogInformation($"Сообщение с id: {id} не было успешно изменено: были введены некорректные данные");
            return View();
        }
    }
}
